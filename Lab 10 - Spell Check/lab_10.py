import re

def split_line(line):
    return re.findall('[A-Za-z]+(?:\'[A-Za-z]+)?',line)

def main():
    dictionary_file = open("dictionary.txt")

    dic_list = []

    for line in dictionary_file:
        line = line.strip()
        dic_list.append(line)

    dictionary_file.close()

    line_number = 0

    print("--- Linear Search ---")

    # Opening Alice file
    alice_file = open("AliceInWonderLand200.txt")

    for line in alice_file:
        word_list = split_line(line)
        line_number += 1
        for word in word_list:
            current_list_position = 0
            while current_list_position < len(dic_list) and dic_list[current_list_position] != word.upper():
                current_list_position += 1
            if current_list_position >= len(dic_list):
                print("Line", line_number, "possible misspelled word: ", word)
    alice_file.close()


    print("--- Binary Search ---")

    lower_bound = current_list_position
    upper_bound = len(dic_list) - 1
    found = False
    alice_file = open("AliceInWonderLand200.txt")

    for line in alice_file:
        word_list = split_line(line)
        line_number += 1
        for word in word_list:
            current_list_position = 0
            # Loop until we find the item, or our upper/lower bounds meet
            while lower_bound <= upper_bound and not found:

                        # Find the middle position
                middle_pos = (lower_bound + upper_bound) // 2

                if dic_list[middle_pos] < word.upper():
                    lower_bound = middle_pos + 1
                elif dic_list[middle_pos] > word.upper():
                    upper_bound = middle_pos - 1
                else:
                    found = True

                if found:
                    print("Line", line_number, "possible misspelled word: ", middle_pos)
    alice_file.close()



main()
