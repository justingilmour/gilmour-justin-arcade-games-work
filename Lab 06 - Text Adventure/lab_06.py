
def main():
    # Append and create room list
    room_list = []

    # Dungeon
    room = ["You are in a dungeon cell. Your cell door has been left unlocked. There is a door"
            " to the north and to the east.", 3, 1, None, None]
    room_list.append(room)

    # Crypt 1
    room = ["There are statues of lords and ladies down the entire corridor. There are stairs ascending to the south."
            " There is a door to the east. The crypts continue on to the north and you dungeon cell is to the west.",
            4, 2, 7, 0]
    room_list.append(room)

    # Storage
    room = ["This room is messy. Random items are scattered all over the room. The only door here is the one you"
            " entered through to the west.", None, None, None, 1]
    room_list.append(room)

    # Dungeons
    room = ["You're still in the dungeons but there are others. There is a door to your west and the dungeons to your"
            " south.", None, 4, 0, None]
    room_list.append(room)

    # Crypt 2
    room = ["There are three statues at the end of the corridor, two lords and a lady. There is door to your east and"
            " the crypts to your south. The dungeons are also to the west.",
            None, 5, 1, 3]
    room_list.append(room)

    # Armory
    room = ["This must be the armory. There are swords and armor lining the walls of the room. The only door is the"
            " door which you came through to the west.", None, None, None, 4]
    room_list.append(room)

    # Dog cages
    room = ["Cages line the walls with dogs barking viciously. There is only one way out and that is the way you came"
            " in, to the east.", None, 7, None, None]
    room_list.append(room)

    # Hallway
    room = ["The hallway is short with options on each end and side.", 9, 8, 1, 6]
    room_list.append(room)

    # Kitchen
    room = ["Pots and pans hang from the ceiling and the smell of fresh food fills your stomach. There is food all"
            " over. An exit is where you entered to the west.", None, None, None, 7]
    room_list.append(room)

    # Hunting Gate
    room = ["You can escape through the gate to the north or you can turn back and go south.", 10, None, 7, None]
    room_list.append(room)

    # Escape
    room = ["You have escaped through the hunting gate! You win!", None, None, 9, None]
    room_list.append(room)

    current_room = 0

    done = False

    # While loop to ask the user what they want to do
    while not done:
        print()
        print(room_list[current_room][0])

        direction = input("What do you want to do? ")

        if direction.lower() == "n" or direction.lower() == "north":
            next_room = room_list[current_room][1]

            if next_room == None:
                print()
                print("You can't go that way.")
            else:
                current_room = next_room

        elif direction.lower() == "e" or direction.lower() == "east":
            next_room = room_list[current_room][2]

            if next_room == None:
                print()
                print("You can't go that way.")
            else:
                current_room = next_room

        elif direction.lower() == "s" or direction.lower() == "south":
            next_room = room_list[current_room][3]

            if next_room == None:
                print()
                print("You can't go that way.")
            else:
                current_room = next_room

        elif direction.lower() == "w" or direction.lower() == "west":
            next_room = room_list[current_room][4]

            if next_room == None:
                print()
                print("You can't go that way.")
            else:
                current_room = next_room

        elif room_list[10]:
            done = True





main()

