""" Final Project """

import arcade
import random
import math


# --- Constants ---
SCREEN_HEIGHT = 800
SCREEN_WIDTH = 800

SPRITE_SCALING_PLAYER = 0.25
SPRITE_SCALING_ENEMY = 0.02
SPRITE_SCALING_LASER = 0.5
SPRITE_SCALING_STAR = 0.5
STAR_COUNT = 150
ENEMY_COUNT = 10

BULLET_SPEED = 5

EXPLOSION_TEXTURE_COUNT = 60

INSTRUCTIONS_PAGE_0 = 0
GAME_RUNNING = 2
GAME_OVER = 3
GAME_WIN = 4


class Explosion(arcade.Sprite):

    explosion_textures = []

    def __init__(self, texture_list):
        super().__init__("images/explosion0000.png")

        # Start at the first frame
        self.current_texture = 0
        self.textures = texture_list

    def update(self):

        # Update to the next frame of the animation. If we are at the end
        # of our frames, then delete this sprite.
        # Explosion graphics from http://www.explosiongenerator.com/
        self.current_texture += 1
        if self.current_texture < len(self.textures):
            self.set_texture(self.current_texture)
        else:
            self.kill()


# Enemy pattern for level 1
class Enemy(arcade.Sprite):

    def update(self):

        self.center_x += self.change_x
        self.center_y += self.change_y

        if self.right > 350:
            self.change_x = 0
            self.change_y = -2

        if self.bottom < 350:
            self.change_x = -2
            self.change_y = 0

        if self.right < 0 and self.top < 400:
            self.kill()


# Boba Fett boss ship level 3
class Enemy2(arcade.Sprite):

    def update(self):

        self.center_x += self.change_x
        self.center_y += self.change_y

        if self.center_y < 701:
            self.change_x = 0
            self.change_y = 0


# Enemy pattern level 3
class Enemy3(arcade.Sprite):

    def update(self):

        self.center_x += self.change_x
        self.center_y += self.change_y

        if self.top < 550:
            self.change_x = 2
            self.change_y = 0

        if self.right > 600:
            self.change_x = 0
            self.change_y = -2

        if self.bottom < 350:
            self.change_x = -2
            self.change_y = 0

        if self.right < 0:
            self.kill()


# Enemy pattern level 3
class Enemy4(arcade.Sprite):

    def update(self):

        self.center_x += self.change_x
        self.center_y += self.change_y

        if self.top < 510:
            self.change_x = -2
            self.change_y = 0

        if self.left < 100:
            self.change_x = 0
            self.change_y = -2

        if self.bottom < 310:
            self.change_x = 2
            self.change_y = 0

        if self.left > SCREEN_WIDTH:
            self.kill()


# Enemy pattern level 2 (didn't realize there was no Enemy5 haha)
class Enemy6(arcade.Sprite):

    def update(self):

        self.center_x += self.change_x
        self.center_y += self.change_y

        if self.right > 400:
            self.change_x = -2
            self.change_y = -2

        if self.right < 0 and self.top < 400:
            self.kill()


# Enemy pattern level 2
class Enemy7(arcade.Sprite):

    def update(self):

        self.center_x += self.change_x
        self.center_y += self.change_y

        if self.left < 400:
            self.change_x = 2
            self.change_y = -2

        if self.left > SCREEN_WIDTH and self.top < 400:
            self.kill()


# Enemy pattern for level 1
class Enemy8(arcade.Sprite):

    def update(self):

        self.center_x += self.change_x
        self.center_y += self.change_y

        if self.left < 450:
            self.change_x = 0
            self.change_y = -2

        if self.bottom < 350:
            self.change_x = 2
            self.change_y = 0

        if self.left > SCREEN_WIDTH and self.top < 400:
            self.kill()


class Star(arcade.Sprite):

    def reset_pos(self):

        self.center_y = random.randrange(SCREEN_HEIGHT + 20,
                                         SCREEN_HEIGHT + 100)
        self.center_x = random.randrange(SCREEN_WIDTH)

    def update(self):
        self.center_y -= 1

        if self.top < 0:
            self.reset_pos()


class MyGame(arcade.Window):

    def __init__(self):

        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "Escaping the Death Star")

        self.current_state = INSTRUCTIONS_PAGE_0

        self.frame_count = 0

        # Sprite List
        self.player_list = None
        self.enemy_list = None
        self.boss_list = None
        self.star_list = None
        self.player_bullet_list = None
        self.enemy_bullet_list = None
        self.pause = False

        # Player Sprite
        self.player_sprite = None
        self.lives = 3
        self.score = 0
        self.boss_lives = 3

        self.level = 1

        # Enemy Sprite
        self.enemy_sprite = None

        # No mouse showing
        self.set_mouse_visible(False)

        self.explosion_texture_list = []

        self.instructions = []
        texture = arcade.load_texture("video_game.png")
        self.instructions.append(texture)

        texture = arcade.load_texture("video_game.png")
        self.instructions.append(texture)

        # http://www.sa-matra.net/sounds/starwars/TIE-Fire.wav
        # falcon_sound, R2D2_sound, and explode_sound all found https://www.soundboard.com/sb/starwarsfx
        self.falcon_sound = arcade.load_sound("fire.mp3")
        self.tiefighter_sound = arcade.load_sound("tiefighter_sound.wav")
        self.R2D2_sound = arcade.load_sound("R2D2.mp3")
        self.explode_sound = arcade.load_sound("explode.mp3")

        for i in range(EXPLOSION_TEXTURE_COUNT):

            texture_name = f"images/explosion{i:04d}.png"

            self.explosion_texture_list.append(arcade.load_texture(texture_name))

        # Background
        arcade.set_background_color(arcade.color.BLACK)

    def level_1(self):

        # Create Enemy
        # Tie fighter image found on Kisspng (https://www.kisspng.com/png-star-wars-png-63641/)
        for i in range(ENEMY_COUNT):

            self.enemy_sprite = Enemy("tie_fighter.png", SPRITE_SCALING_ENEMY)

            self.enemy_sprite.center_x = 0 + (i * -60)
            self.enemy_sprite.center_y = 600

            self.enemy_sprite.change_x = 2
            self.enemy_sprite.change_y = 0

            self.enemy_list.append(self.enemy_sprite)

        for i in range(ENEMY_COUNT):

            self.enemy_sprite = Enemy8("tie_fighter.png", SPRITE_SCALING_ENEMY)

            self.enemy_sprite.center_x = 800 + (i * 60)
            self.enemy_sprite.center_y = 600

            self.enemy_sprite.change_x = -2
            self.enemy_sprite.change_y = 0

            self.enemy_list.append(self.enemy_sprite)

    def level_2(self):

        for i in range(ENEMY_COUNT):

            self.enemy_sprite = Enemy6("tie_fighter.png", SPRITE_SCALING_ENEMY)

            self.enemy_sprite.center_x = 0 + (i * -40)
            self.enemy_sprite.center_y = 800 + (i * 40)

            self.enemy_sprite.change_x = 2
            self.enemy_sprite.change_y = -2

            self.enemy_list.append(self.enemy_sprite)

        for i in range(ENEMY_COUNT):

            self.enemy_sprite = Enemy7("tie_fighter.png", SPRITE_SCALING_ENEMY)

            self.enemy_sprite.center_x = 800 + (i * 40)
            self.enemy_sprite.center_y = 800 + (i * 40)

            self.enemy_sprite.change_x = -2
            self.enemy_sprite.change_y = -2

            self.enemy_list.append(self.enemy_sprite)

    def level_3(self):

        # Boba Fett image found on Kisspng
        # https://www.kisspng.com/png-boba-fett-jango-fett-star-wars-the-clone-wars-slav-1209121/
        self.enemy_sprite = Enemy2("slave_1.png", SPRITE_SCALING_ENEMY + .02)

        self.enemy_sprite.center_x = 400
        self.enemy_sprite.center_y = 850

        self.enemy_sprite.change_x = 0
        self.enemy_sprite.change_y = -1

        self.boss_list.append(self.enemy_sprite)

        for i in range(ENEMY_COUNT):

            self.enemy_sprite = Enemy3("tie_fighter.png", SPRITE_SCALING_ENEMY)

            self.enemy_sprite.center_x = 200
            self.enemy_sprite.center_y = 800 + (i * 60)

            self.enemy_sprite.change_x = 0
            self.enemy_sprite.change_y = -2

            self.enemy_list.append(self.enemy_sprite)

        for i in range(ENEMY_COUNT):

            self.enemy_sprite = Enemy4("tie_fighter.png", SPRITE_SCALING_ENEMY)

            self.enemy_sprite.center_x = 600
            self.enemy_sprite.center_y = 800 + (i * 60)

            self.enemy_sprite.change_x = 0
            self.enemy_sprite.change_y = -2

            self.enemy_list.append(self.enemy_sprite)

    def setup(self):

        # Score
        self.lives = 3
        self.score = 0

        self.level = 1

        # Sprite List
        self.player_list = arcade.SpriteList()
        self.enemy_list = arcade.SpriteList()
        self.boss_list = arcade.SpriteList()
        self.star_list = arcade.SpriteList()
        self.player_bullet_list = arcade.SpriteList()
        self.enemy_bullet_list = arcade.SpriteList()
        self.explosions_list = arcade.SpriteList()

        # Create Player
        # Millennium Falcon image found on PNG Arts (https://www.pngarts.com/explore/71471)
        self.player_sprite = arcade.Sprite("millennium_falcon.png", SPRITE_SCALING_PLAYER)
        self.player_sprite.center_x = 100
        self.player_sprite.center_y = 100
        self.player_list.append(self.player_sprite)

        self.level_1()

        # Create the stars
        for i in range(STAR_COUNT):

            # Star image found from Kenney (https://kenney.nl/assets/space-shooter-redux)
            star = Star("star1.png", SPRITE_SCALING_STAR)

            star.center_x = random.randrange(SCREEN_WIDTH)
            star.center_y = random.randrange(SCREEN_HEIGHT)

            self.star_list.append(star)

    def draw_instructions_page(self, page_number):
        page_texture = self.instructions[page_number]
        arcade.draw_texture_rectangle(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2,
                                      page_texture.width,
                                      page_texture.height, page_texture, 0)

    def draw_game_over(self):

        output = "Game Over"
        arcade.draw_text(output, 220, 400, arcade.color.WHITE, 50, bold=True)

        output = f"Your score was: {self.score}"
        arcade.draw_text(output, 280, 350, arcade.color.WHITE, 20)

        output = "Click to restart"
        arcade.draw_text(output, 300, 300, arcade.color.WHITE, 20)

    def draw_you_win(self):

        output = "YOU WIN!"
        arcade.draw_text(output, 245, 400, arcade.color.WHITE, 50, bold=True)

        output = f"Your score was: {self.score}"
        arcade.draw_text(output, 280, 350, arcade.color.WHITE, 20)

        output = "Click to play again"
        arcade.draw_text(output, 290, 300, arcade.color.WHITE, 20)

    def draw_game(self):

        # Draw Sprites
        self.star_list.draw()
        self.player_list.draw()
        self.enemy_list.draw()
        self.boss_list.draw()
        self.player_bullet_list.draw()
        self.enemy_bullet_list.draw()
        self.explosions_list.draw()

        output = f"Lives left: {self.lives}"
        arcade.draw_text(output, 10, 20, arcade.color.RED, 14)

        output = f"Score: {self.score}"
        arcade.draw_text(output, 10, 40, arcade.color.RED, 14)

    def on_draw(self):

        arcade.start_render()

        if self.current_state == INSTRUCTIONS_PAGE_0:
            self.draw_instructions_page(0)
            self.set_mouse_visible(True)

        elif self.current_state == GAME_RUNNING:
            self.draw_game()
            self.set_mouse_visible(False)

        elif self.current_state == GAME_WIN:
            self.draw_you_win()

        else:
            self.draw_game()
            self.draw_game_over()

    def on_mouse_motion(self, x, y, dx, dy):

        if self.current_state == GAME_RUNNING:
            self.player_sprite.center_x = x
            # self.player_sprite.center_y = y

    def on_mouse_press(self, x, y, button, modifiers):
        
        # Create laser
        # Laser image found from Kenney (https://kenney.nl/assets/space-shooter-redux)
        bullet = arcade.Sprite("laserBlue16.png", SPRITE_SCALING_LASER)

        if self.lives != 0:
            bullet.center_x = self.player_sprite.center_x
            bullet.center_y = self.player_sprite.center_y + 30
            bullet.change_y = BULLET_SPEED
            arcade.play_sound(self.falcon_sound)

        # Add to list
        self.player_bullet_list.append(bullet)

        if self.current_state == INSTRUCTIONS_PAGE_0:
            self.current_state = GAME_RUNNING
        elif self.current_state == GAME_OVER:
            # Restart the game.
            self.setup()
            self.current_state = GAME_RUNNING
        elif self.current_state == GAME_WIN:
            self.setup()
            self.current_state = GAME_RUNNING

    def update(self, delta_time):

        if self.current_state == GAME_RUNNING:
            self.star_list.update()
            self.enemy_list.update()
            self.boss_list.update()
            self.player_bullet_list.update()
            self.enemy_bullet_list.update()
            self.explosions_list.update()

            self.frame_count += 1

            for enemy in self.enemy_list:

                if random.randrange(100) == 0:
                    enemy_bullet = arcade.Sprite("laserRed16.png", SPRITE_SCALING_LASER)
                    enemy_bullet.center_x = enemy.center_x
                    enemy_bullet.angle = -180
                    enemy_bullet.top = enemy.bottom
                    enemy_bullet.change_y = -BULLET_SPEED
                    arcade.play_sound(self.tiefighter_sound)
                    self.enemy_bullet_list.append(enemy_bullet)

            for bullet in self.player_bullet_list:

                good_hit_list = arcade.check_for_collision_with_list(bullet, self.enemy_list)
                boss_hit_list = arcade.check_for_collision_with_list(bullet, self.boss_list)

                if len(good_hit_list) > 0:
                    explosion = Explosion(self.explosion_texture_list)
                    explosion.center_x = good_hit_list[0].center_x
                    explosion.center_y = good_hit_list[0].center_y
                    self.explosions_list.append(explosion)
                    arcade.play_sound(self.explode_sound)
                    bullet.kill()

                if len(boss_hit_list) > 0:
                    explosion = Explosion(self.explosion_texture_list)
                    explosion.center_x = boss_hit_list[0].center_x
                    explosion.center_y = boss_hit_list[0].center_y
                    self.explosions_list.append(explosion)
                    arcade.play_sound(self.explode_sound)
                    bullet.kill()

                for enemy in good_hit_list:
                    enemy.kill()
                    self.score += 10

                for boss in boss_hit_list:
                    self.boss_lives -= 1

                    if self.boss_lives == 0:
                        boss.kill()
                        self.score += 50

                if len(self.enemy_list) == 0 and len(self.boss_list) == 0 and self.level == 3:
                    self.current_state = GAME_WIN
                    self.set_mouse_visible(True)

                if bullet.bottom > SCREEN_HEIGHT:
                    bullet.kill()

            for enemy_bullet in self.enemy_bullet_list:

                bad_hit_list = arcade.check_for_collision_with_list(enemy_bullet, self.player_list)

                for self.player_sprite in bad_hit_list:
                    arcade.play_sound(self.R2D2_sound)
                    self.lives -= 1
                    enemy_bullet.kill()

                if self.lives == 0:
                    self.current_state = GAME_OVER
                    self.set_mouse_visible(True)

            for boss in self.boss_list:

                start_x = boss.center_x
                start_y = boss.center_y

                # Get the destination location for the bullet
                dest_x = self.player_sprite.center_x
                dest_y = self.player_sprite.center_y

                # Do math to calculate how to get the bullet to the destination.
                # Calculation the angle in radians between the start points
                # and end points. This is the angle the bullet will travel.
                x_diff = dest_x - start_x
                y_diff = dest_y - start_y
                angle = math.atan2(y_diff, x_diff)

                # Set the enemy to face the player.
                boss.angle = math.degrees(angle) + 90

                # Shoot every 60 frames change of shooting each frame
                if self.frame_count % 60 == 0:
                    bullet = arcade.Sprite("laserRed16.png", SPRITE_SCALING_LASER)
                    bullet.center_x = start_x
                    bullet.center_y = start_y

                    # Angle the bullet sprite
                    bullet.angle = math.degrees(angle) - 90

                    # Taking into account the angle, calculate our change_x
                    # and change_y. Velocity is how fast the bullet travels.
                    bullet.change_x = math.cos(angle) * BULLET_SPEED
                    bullet.change_y = math.sin(angle) * BULLET_SPEED

                    self.enemy_bullet_list.append(bullet)

            for bullet in self.enemy_bullet_list:
                if bullet.top < 0:
                    bullet.kill()

            if len(self.enemy_list) == 0 and self.level == 1:
                self.level += 1
                self.level_2()

            elif len(self.enemy_list) == 0 and self.level == 2:
                self.level += 1
                self.level_3()


def main():
    window = MyGame()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
