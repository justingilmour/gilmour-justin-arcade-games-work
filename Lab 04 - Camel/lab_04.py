# Random number simulator
import random

def main():
    # Introduction to the game
    print()
    print("Welcome to Camel!")
    print("You have stolen a camel to make your way across the great Mobi Desert.")
    print("The natives want their camel back and are chasing you down! Survive your")
    print("desert trek and out run the natives.")
    print()

    # Set variables to starting numbers
    done = False
    miles_traveled = 0
    thirst = 0
    camel_tiredness = 0
    native_location = -20
    full_canteen = 3

    # Main loop of instructions
    while not done:
        print("A. Drink from your canteen.")
        print("B. Ahead moderate speed.")
        print("C. Ahead full speed.")
        print("D. Stop for the night.")
        print("E. Status check.")
        print("Q. Quit.")
        print()

        # Enter the users answer
        user_choice = input("What is your choice? ")
        if user_choice.upper() == "Q":
            done = True
            print()
            print("You have quit the game.")

        # Give a status update.
        elif user_choice.upper() == "E":
            print()
            print("Miles traveled:", miles_traveled)
            print("Drinks in canteen:", full_canteen)
            print("The natives are", miles_traveled - native_location, "miles behind you.")
            print()

        # Stop and rest for the night
        elif user_choice.upper()== "D":
            camel_tiredness = 0
            print()
            print("The camel is happy :)")
            print()
            native_location += random.randrange(7, 15)

        # Go ahead full speed
        elif user_choice.upper() == "C":
            full_speed = random.randrange(10, 21)
            print()
            print("You traveled", full_speed, "more miles.")
            print()
            miles_traveled += full_speed
            thirst += 1
            camel_tiredness += random.randrange(1, 4)
            native_location += random.randrange(7, 15)
            if random.randrange(1, 21) == 7:
                print()
                print("You found an oasis!!")
                print("Your canteen has been filled, you are not thirsty and the camel has been rested!")
                print()
                camel_tiredness = 0
                thirst = 0
                full_canteen = 3

        # Go ahead at moderate speed
        elif user_choice.upper() == "B":
            moderate_speed = random.randrange(5, 13)
            print()
            print("You traveled", moderate_speed, "more miles.")
            print()
            miles_traveled += moderate_speed
            thirst += 1
            camel_tiredness += 1
            native_location += random.randrange(7, 15)
            if random.randrange(1, 21) == 7:
                print()
                print("You found an oasis!!")
                print("Your canteen has been filled, you are not thirsty and the camel has been rested!")
                print()
                camel_tiredness = 0
                thirst = 0
                full_canteen = 3


        # Drink from the canteen
        elif user_choice.upper() == "A":
            if full_canteen > 0:
                full_canteen -= 1
                thirst = 0
                print()
                print("You have quenched your thirst. For now.")
                print("You have", full_canteen, "drinks left.")
                print()
            elif full_canteen == 0:
                print()
                print("You have no water left :(")
                print()

        # Made it across the desert
        if not done and miles_traveled >= 200:
            print("You made it across the desert!")
            print("You win :)")
            done = True

        # Natives are getting close if statements
        if native_location >= miles_traveled:
            print("The natives caught you!!")
            print("You lose :(")
            done = True

        # Camel is getting tired if statements
        if camel_tiredness > 8:
            print("Your camel is dead!")
            print("You lose :(")
            done = True
        elif not done and camel_tiredness > 5:
            print("Your camel is getting tired.")
            print()

        # Character is getting thirsty if statements
        if thirst >6:
            print("You have died of thirst!!")
            print("You lose :(")
            done = True
        elif not done and thirst > 4:
            print("You are thirsty.")
            print()

        # Natives are getting closer
        elif not done and miles_traveled - native_location < 15:
            print("The natives are getting close!")
            print()

main()
