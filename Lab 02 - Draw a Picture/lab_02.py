# Import arcade to run the program.

import arcade

# Insert screen height and width as variables.

SCREENWIDTH = 700
SCREENHEIGHT = 850

arcade.open_window(SCREENWIDTH, SCREENHEIGHT, "Spongebob's Pineapple")

arcade.set_background_color(arcade.color.FRENCH_SKY_BLUE)

arcade.start_render()

# Draw sand and sea components
arcade.draw_rectangle_filled(350, 150, 700, 300, arcade.color.DESERT_SAND)

# Draw the leaves of the pineapple
arcade.draw_polygon_filled([[450, 500],
                            [250, 500],
                            [250, 750],
                            [300, 675],
                            [350, 750],
                            [400, 675],
                            [450, 750],
                            [450, 500]],
                           arcade.color.DARK_GREEN)

# Draw the orange pineapple and lines
arcade.draw_ellipse_filled(350, 400, 170, 225, arcade.color.ORANGE)
arcade.draw_line(350, 625, 200, 500, arcade.color.IMPERIAL_BLUE)
arcade.draw_line(450, 582.5, 182.5, 360, arcade.color.IMPERIAL_BLUE)
arcade.draw_line(500, 505, 220, 260, arcade.color.IMPERIAL_BLUE)
arcade.draw_line(520, 400, 320, 215, arcade.color.IMPERIAL_BLUE)
arcade.draw_line(350, 625, 500, 500, arcade.color.IMPERIAL_BLUE)
arcade.draw_line(250, 582.5, 517.5, 360, arcade.color.IMPERIAL_BLUE)
arcade.draw_line(200, 505, 480, 260, arcade.color.IMPERIAL_BLUE)
arcade.draw_line(180, 400, 380, 215, arcade.color.IMPERIAL_BLUE)

# Draw the door
arcade.draw_ellipse_filled(350, 250, 65, 125, arcade.color.LAVENDER_GRAY)
arcade.draw_rectangle_filled(350, 100, 400, 225, arcade.color.DESERT_SAND)
arcade.draw_circle_filled(350, 275, 20, arcade.color.ONYX)
arcade.draw_line(350, 275, 350, 315, arcade.color.ONYX)
arcade.draw_line(350, 275, 310, 275, arcade.color.ONYX)
arcade.draw_line(350, 275, 350, 235, arcade.color.ONYX)
arcade.draw_line(350, 275, 390, 275, arcade.color.ONYX)

# Draw bubbler and bubbles on side of the pineapple
arcade.draw_rectangle_filled(500, 475, 100, 30, arcade.color.LAVENDER_GRAY)
arcade.draw_rectangle_filled(550, 490, 20, 60, arcade.color.LAVENDER_GRAY)
arcade.draw_rectangle_filled(550, 530, 40, 20, arcade.color.LAVENDER_GRAY)
arcade.draw_circle_filled(455, 360, 45, arcade.color.IMPERIAL_BLUE)
arcade.draw_circle_filled(455, 360, 25, arcade.color.WATERSPOUT)
arcade.draw_circle_filled(260, 450, 45, arcade.color.IMPERIAL_BLUE)
arcade.draw_circle_filled(260, 450, 25, arcade.color.WATERSPOUT)
arcade.draw_circle_outline(550, 570, 10, arcade.color.BUBBLES)
arcade.draw_circle_outline(570, 590, 7.5, arcade.color.BUBBLES)
arcade.draw_circle_outline(530, 610, 7.5, arcade.color.BUBBLES)

# Draw sidewalk and text/title
arcade.draw_polygon_filled([[250, 1],
                            [290, 210],
                            [410, 210],
                            [450, 1],
                            [250, 1]],
                           arcade.color.LICORICE)
arcade.draw_text("Who lives in a Pineapple under the Sea?", 120, 820, arcade.color.IMPERIAL, 20)

# Draw Spongebob
arcade.draw_rectangle_filled(600, 300, 60, 60, arcade.color.BANANA_YELLOW)
arcade.draw_rectangle_filled(600, 260, 60, 20, arcade.color.BROWN)
arcade.draw_line(580, 250, 580, 230, arcade.color.BANANA_YELLOW)
arcade.draw_line(620, 250, 620, 230, arcade.color.BANANA_YELLOW)
arcade.draw_rectangle_filled(580, 228, 15, 10, arcade.color.BLACK)
arcade.draw_rectangle_filled(620, 228, 15, 10, arcade.color.BLACK)
arcade.draw_line(630, 300, 650, 300, arcade.color.BANANA_YELLOW)
arcade.draw_line(650, 300, 660,310, arcade.color.BANANA_YELLOW)
arcade.draw_line(570, 300, 550, 300, arcade.color.BANANA_YELLOW)
arcade.draw_line(550, 300, 540, 310, arcade.color.BANANA_YELLOW)
arcade.draw_ellipse_filled(590, 310, 7, 10, arcade.color.WHITE)
arcade.draw_ellipse_filled(605, 310, 7, 10, arcade.color.WHITE)
arcade.draw_ellipse_filled(590, 310, 4, 7, arcade.color.BLACK)
arcade.draw_ellipse_filled(605, 310, 4, 7, arcade.color.BLACK)
arcade.draw_ellipse_filled(590, 310, 2, 4, arcade.color.BLUE)
arcade.draw_ellipse_filled(605, 310, 2, 4, arcade.color.BLUE)
arcade.draw_line(585, 285, 615, 285, arcade.color.BLACK)
arcade.draw_rectangle_filled(595, 282, 7, 7, arcade.color.WHITE)
arcade.draw_rectangle_filled(605, 282, 7, 7, arcade.color.WHITE)

# Draw Patrick
arcade.draw_polygon_filled([[100, 350],
                            [120, 300],
                            [120, 300],
                            [115, 210],
                            [100, 250],
                            [85, 210],
                            [90, 260],
                            [40, 310],
                            [80, 300]],
                           arcade.color.DARK_PINK)

# Finish rendering 
arcade.finish_render()

arcade.run()










