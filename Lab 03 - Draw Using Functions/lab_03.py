# Import arcade library
import arcade

# Insert screen height and width as variables.

SCREENWIDTH = 700
SCREENHEIGHT = 850

def main():
    arcade.open_window(SCREENWIDTH, SCREENHEIGHT, "Spongebob's Pineapple")
    arcade.set_background_color(arcade.color.FRENCH_SKY_BLUE)

    arcade.schedule(on_draw, 1/60)
    arcade.run()


def draw_sand_title():
    arcade.draw_rectangle_filled(350, 150, 700, 300, arcade.color.DESERT_SAND)
    arcade.draw_text("Who lives in a Pineapple under the Sea?", 120, 820, arcade.color.IMPERIAL, 20)

def draw_pineapple():
    arcade.draw_polygon_filled([[450, 500],
                                [250, 500],
                                [250, 750],
                                [300, 675],
                                [350, 750],
                                [400, 675],
                                [450, 750],
                                [450, 500]],
                               arcade.color.DARK_GREEN)
    arcade.draw_ellipse_filled(350, 400, 170, 225, arcade.color.ORANGE)
    arcade.draw_line(350, 625, 200, 500, arcade.color.IMPERIAL_BLUE)
    arcade.draw_line(450, 582.5, 182.5, 360, arcade.color.IMPERIAL_BLUE)
    arcade.draw_line(500, 505, 220, 260, arcade.color.IMPERIAL_BLUE)
    arcade.draw_line(520, 400, 320, 215, arcade.color.IMPERIAL_BLUE)
    arcade.draw_line(350, 625, 500, 500, arcade.color.IMPERIAL_BLUE)
    arcade.draw_line(250, 582.5, 517.5, 360, arcade.color.IMPERIAL_BLUE)
    arcade.draw_line(200, 505, 480, 260, arcade.color.IMPERIAL_BLUE)
    arcade.draw_line(180, 400, 380, 215, arcade.color.IMPERIAL_BLUE)

def draw_door_sidewalk(x, x2, y, y2):
    arcade.draw_ellipse_filled(350, 250, 65, 125, arcade.color.LAVENDER_GRAY)
    arcade.draw_rectangle_filled(350, 100, 400, 225, arcade.color.DESERT_SAND)
    arcade.draw_polygon_filled([[250, 1],
                                [290, 210],
                                [410, 210],
                                [450, 1],
                                [250, 1]],
                               arcade.color.LICORICE)
    arcade.draw_circle_filled(350, 275, 20, arcade.color.ONYX)
    arcade.draw_line(x, x2, 50 + y, 115 + y2, arcade.color.ONYX)
    arcade.draw_line(x, x2, 10 + y, 75 + y2, arcade.color.ONYX)
    arcade.draw_line(x, x2, 50 + y, 35 + y2, arcade.color.ONYX)
    arcade.draw_line(x, x2, 90 + y, 75 + y2, arcade.color.ONYX)

def draw_bubbler_windows(x, y):
    arcade.draw_rectangle_filled(500, 475, 100, 30, arcade.color.LAVENDER_GRAY)
    arcade.draw_rectangle_filled(550, 490, 20, 60, arcade.color.LAVENDER_GRAY)
    arcade.draw_rectangle_filled(550, 530, 40, 20, arcade.color.LAVENDER_GRAY)
    arcade.draw_circle_filled(x + 155,y - 140, 45, arcade.color.IMPERIAL_BLUE)
    arcade.draw_circle_filled(x + 155,y - 140, 25, arcade.color.WATERSPOUT)
    arcade.draw_circle_filled(x - 40,y - 50, 45, arcade.color.IMPERIAL_BLUE)
    arcade.draw_circle_filled(x - 40,y - 50, 25, arcade.color.WATERSPOUT)
    arcade.draw_circle_outline(x + 250, y + 70, 10, arcade.color.BUBBLES)
    arcade.draw_circle_outline(x + 270, y + 90, 7.5, arcade.color.BUBBLES)
    arcade.draw_circle_outline(x + 230,y + 110, 7.5, arcade.color.BUBBLES)

def draw_spongebob(x, y):
    arcade.draw_rectangle_filled(x + 20, y + 50, 60, 60, arcade.color.BANANA_YELLOW)
    arcade.draw_rectangle_filled(x + 20, y + 10, 60, 20, arcade.color.BROWN)
    arcade.draw_line(x, y, x, y - 20, arcade.color.BANANA_YELLOW)
    arcade.draw_line(x + 40, y, x + 40, y - 20, arcade.color.BANANA_YELLOW)
    arcade.draw_rectangle_filled(x, y - 22, 15, 10, arcade.color.BLACK)
    arcade.draw_rectangle_filled(x + 40, y - 22, 15, 10, arcade.color.BLACK)
    arcade.draw_line(x + 50, y + 50, x + 70, y + 50, arcade.color.BANANA_YELLOW)
    arcade.draw_line(x + 70, y + 50, x + 80, y + 60, arcade.color.BANANA_YELLOW)
    arcade.draw_line(x - 10, y + 50, x - 30, y + 50, arcade.color.BANANA_YELLOW)
    arcade.draw_line(x - 30, y + 50, x - 40, y + 60, arcade.color.BANANA_YELLOW)
    arcade.draw_ellipse_filled(x + 10, y + 60, 7, 10, arcade.color.WHITE)
    arcade.draw_ellipse_filled(x + 25, y + 60, 7, 10, arcade.color.WHITE)
    arcade.draw_ellipse_filled(x + 10, y + 60, 4, 7, arcade.color.BLACK)
    arcade.draw_ellipse_filled(x + 25, y + 60, 4, 7, arcade.color.BLACK)
    arcade.draw_ellipse_filled(x + 10, y + 60, 2, 4, arcade.color.BLUE)
    arcade.draw_ellipse_filled(x + 25, y + 60, 2, 4, arcade.color.BLUE)
    arcade.draw_line(x + 5, y + 35, x + 35, y + 35, arcade.color.BLACK)
    arcade.draw_rectangle_filled(x + 15, y + 32, 7, 7, arcade.color.WHITE)
    arcade.draw_rectangle_filled(x + 25, y + 32, 7, 7, arcade.color.WHITE)

def on_draw(delta_time):
    arcade.start_render()

    draw_sand_title()
    draw_pineapple()
    draw_door_sidewalk(350, 275, 300, 200)
    draw_bubbler_windows(300, 500)
    draw_spongebob(on_draw.spongebob_x, 235)

    if on_draw.spongebob_x > 330:
        on_draw.spongebob_x = -50

    on_draw.spongebob_x += 1

on_draw.spongebob_x = -50

main()
