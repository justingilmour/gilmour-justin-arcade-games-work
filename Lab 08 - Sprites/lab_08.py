""" Sprite Sample Program """

import random
import arcade

# --- Constants ---
SPRITE_SCALING_GHOST = 0.015
SPRITE_SCALING_PLAYER = 0.085
SPRITE_SCALING_BONE = 0.1
BONE_COUNT = 50
GHOST_COUNT = 30

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


class Bone(arcade.Sprite):

    def __init__(self, filename, sprite_scaling):

        super().__init__(filename, sprite_scaling)

        self.change_x = 0
        self.change_y = 0

    def update(self):
        self.center_x += self.change_x
        self.center_y += self.change_y

        # If we are out-of-bounds, then 'bounce'
        if self.left < 0 and self.change_x < 0:
            self.change_x *= -1

        if self.right > SCREEN_WIDTH and self.change_x > 0:
            self.change_x *= -1

        if self.bottom < 0 and self.change_y < 0:
            self.change_y *= -1

        if self.top > SCREEN_HEIGHT and self.change_y > 0:
            self.change_y *= -1

class Ghost(arcade.Sprite):

    def update(self):
        self.center_x -= 1

        if self.left < -20:
            self.center_x = random.randrange(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100)
            self.center_y = random.randrange(SCREEN_HEIGHT)

class MyGame(arcade.Window):
    """ Our custom Window Class"""

    def __init__(self):
        """ Initializer """
        # Call the parent class initializer
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "Scooby-Doo's Snack Hunt")

        # Variables that will hold sprite lists
        self.player_list = None
        self.bone_list = None
        self.ghost_list = None

        # Set up the player info
        self.player_sprite = None
        self.score = 0

        # Don't show the mouse cursor
        self.set_mouse_visible(False)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        """ Set up the game and initialize the variables. """

        # Sprite lists
        self.player_list = arcade.SpriteList()
        self.bone_list = arcade.SpriteList()
        self.ghost_list = arcade.SpriteList()

        # Score
        self.score = 0

        # Set up the player
        self.player_sprite = arcade.Sprite("scoobydoo.png", SPRITE_SCALING_PLAYER)
        self.player_sprite.center_x = 50
        self.player_sprite.center_y = 50
        self.player_list.append(self.player_sprite)

        # Create the coins
        for i in range(BONE_COUNT):

            # Create the coin instance
            bone = Bone("bone.png", SPRITE_SCALING_BONE)

            # Position the bone
            bone.center_x = random.randrange(SCREEN_WIDTH)
            bone.center_y = random.randrange(SCREEN_HEIGHT)

            while bone.change_x == 0 and bone.change_y == 0:
                bone.change_x = random.randrange(-3, 4)
                bone.change_y = random.randrange(-3, 4)

            # Add the coin to the lists
            self.bone_list.append(bone)

        for i in range(GHOST_COUNT):
            ghost = Ghost("ghost.png", SPRITE_SCALING_GHOST)

            ghost.center_x = random.randrange(SCREEN_WIDTH)
            ghost.center_y = random.randrange(SCREEN_HEIGHT)

            self.ghost_list.append(ghost)

    def on_draw(self):
        """ Draw everything """
        arcade.start_render()
        self.bone_list.draw()
        self.player_list.draw()
        self.ghost_list.draw()

        # Put the text on the screen.
        output = f"Score: {self.score}"
        arcade.draw_text(output, 10, 20, arcade.color.BLACK, 14)

        if (len(self.bone_list)) == 0:
            arcade.draw_text("GAME OVER", 200, 300, arcade.color.BLACK, 50, bold=True)

    def on_mouse_motion(self, x, y, dx, dy):
        """ Handle Mouse Motion """

        # Move the center of the player sprite to match the mouse x, y
        if (len(self.bone_list)) != 0:
            self.player_sprite.center_x = x
            self.player_sprite.center_y = y
        elif (len(self.bone_list)) == 0:
            self.set_mouse_visible(True)

    def update(self, delta_time):
        """ Movement and game logic """

        # Call update on all sprites (The sprites don't do much in this
        # example though.)

        if (len(self.bone_list)) != 0:
            self.bone_list.update()
            self.ghost_list.update()

        self.bite_sound = arcade.load_sound("Bite.mp3")
        self.bump_sound = arcade.load_sound("bump.wav")


        # Generate a list of all sprites that collided with the player.
        good_hit = arcade.check_for_collision_with_list(self.player_sprite,
                                                        self.bone_list)

        bad_hit = arcade.check_for_collision_with_list(self.player_sprite, self.ghost_list)

        # Loop through each colliding sprite, remove it, and add to the score.
        for bone in good_hit:
            arcade.play_sound(self.bite_sound)
            bone.kill()
            self.score += 1

            bone.center_y = random.randrange(SCREEN_HEIGHT + 20, SCREEN_HEIGHT + 100)
            bone.center_x = random.randrange(SCREEN_WIDTH)

        for ghost in bad_hit:
            self.score -= 2
            arcade.play_sound(self.bump_sound)

            ghost.center_x = random.randrange(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100)
            ghost.center_y = random.randrange(SCREEN_HEIGHT)


def main():
    """ Main method """
    window = MyGame()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()