""" Lab 7 - User Control """

import arcade

# --- Constants ---
SCREEN_WIDTH = 700
SCREEN_HEIGHT = 800
MOVEMENT_SPEED = 20

class Gary:
    def __init__(self, position_x, position_y, change_x, change_y, radius, color):

        self.position_x = position_x
        self.position_y = position_y
        self.change_x = change_x
        self.change_y = change_y
        self.radius = radius
        self.color = color

    def draw(self):
        arcade.draw_circle_filled(75 + self.position_x, 250 + self.position_y, 20, arcade.color.PINK)
        arcade.draw_rectangle_filled(85 + self.position_x, 228 + self.position_y, 60, 10, arcade.color.INDIGO)



    def update(self):
        self.position_y += self.change_y
        self.position_x += self.change_x

        if self.position_x < self.radius:
            self.position_x = self.radius

        if self.position_x > SCREEN_WIDTH - self.radius:
            self.position_x = SCREEN_WIDTH - self.radius

        if self.position_y < self.radius:
            self.position_y = self.radius

        if self.position_y > SCREEN_HEIGHT - self.radius:
            self.position_y = SCREEN_HEIGHT - self.radius


class Spongebob:
    def __init__(self, position_x, position_y, radius, color):

        self.position_x = position_x
        self.position_y = position_y
        self.radius = radius
        self.color = color

    def draw(self):
        arcade.draw_rectangle_filled(600 + self.position_x - 600, 300 + self.position_y - 300, 60, 60,
                                     arcade.color.BANANA_YELLOW)
        arcade.draw_rectangle_filled(600 + self.position_x - 600, 260 + self.position_y - 300, 60, 20,
                                     arcade.color.BROWN)
        arcade.draw_line(580 + self.position_x - 600, 250 + self.position_y - 300, 580 + self.position_x - 600,
                         230 + self.position_y - 300, arcade.color.BANANA_YELLOW)
        arcade.draw_line(620 + self.position_x - 600, 250 + self.position_y - 300, 620 + self.position_x - 600,
                         230 + self.position_y - 300, arcade.color.BANANA_YELLOW)
        arcade.draw_rectangle_filled(580 + self.position_x - 600, 228 + self.position_y - 300, 15, 10,
                                     arcade.color.BLACK)
        arcade.draw_rectangle_filled(620 + self.position_x - 600, 228 + self.position_y - 300, 15, 10,
                                     arcade.color.BLACK)
        arcade.draw_line(630 + self.position_x - 600, 300 + self.position_y - 300, 650 + self.position_x - 600,
                         300 + self.position_y - 300, arcade.color.BANANA_YELLOW)
        arcade.draw_line(650 + self.position_x - 600, 300 + self.position_y - 300, 660 + self.position_x - 600,
                         310 + self.position_y - 300, arcade.color.BANANA_YELLOW)
        arcade.draw_line(570 + self.position_x - 600, 300 + self.position_y - 300, 550 + self.position_x - 600,
                         300 + self.position_y - 300, arcade.color.BANANA_YELLOW)
        arcade.draw_line(550 + self.position_x - 600, 300 + self.position_y - 300, 540 + self.position_x - 600,
                         310 + self.position_y - 300, arcade.color.BANANA_YELLOW)
        arcade.draw_ellipse_filled(590 + self.position_x - 600, 310 + self.position_y - 300, 7, 10, arcade.color.WHITE)
        arcade.draw_ellipse_filled(605 + self.position_x - 600, 310 + self.position_y - 300, 7, 10, arcade.color.WHITE)
        arcade.draw_ellipse_filled(590 + self.position_x - 600, 310 + self.position_y - 300, 4, 7, arcade.color.BLACK)
        arcade.draw_ellipse_filled(605 + self.position_x - 600, 310 + self.position_y - 300, 4, 7, arcade.color.BLACK)
        arcade.draw_ellipse_filled(590 + self.position_x - 600, 310 + self.position_y - 300, 2, 4, arcade.color.BLUE)
        arcade.draw_ellipse_filled(605 + self.position_x - 600, 310 + self.position_y - 300, 2, 4, arcade.color.BLUE)
        arcade.draw_line(585 + self.position_x - 600, 285 + self.position_y - 300, 615 + self.position_x - 600,
                         285 + self.position_y - 300, arcade.color.BLACK)
        arcade.draw_rectangle_filled(595 + self.position_x - 600, 282 + self.position_y - 300, 7, 7, arcade.color.WHITE)
        arcade.draw_rectangle_filled(605 + self.position_x - 600, 282 + self.position_y - 300, 7, 7, arcade.color.WHITE)


class MyGame(arcade.Window):
    """ Our Custom Window Class"""

    def __init__(self):
        """ Initializer """

        # Call the parent class initializer
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 7 - User Control")

        self.jingle1 = arcade.load_sound("jingles_NESS05.ogg")
        self.jingle2 = arcade.load_sound("jingles_STEEL07.ogg")

        self.set_mouse_visible(False)

        arcade.set_background_color(arcade.color.FRENCH_SKY_BLUE)

        self.spongebob = Spongebob(50, 50, 15, arcade.color.AUBURN)
        self.gary = Gary(50, 50, 0, 0, 25, arcade.color.INDIGO)

    def on_draw(self):
        arcade.start_render()

        # Draw sand and sea components
        arcade.draw_rectangle_filled(350, 150, 700, 300, arcade.color.DESERT_SAND)

        # Draw the leaves of the pineapple
        arcade.draw_polygon_filled([[450, 500],
                                    [250, 500],
                                    [250, 750],
                                    [300, 675],
                                    [350, 750],
                                    [400, 675],
                                    [450, 750],
                                    [450, 500]],
                                   arcade.color.DARK_GREEN)

        # Draw the orange pineapple and lines
        arcade.draw_ellipse_filled(350, 400, 170, 225, arcade.color.ORANGE)
        arcade.draw_line(350, 625, 200, 500, arcade.color.IMPERIAL_BLUE)
        arcade.draw_line(450, 582.5, 182.5, 360, arcade.color.IMPERIAL_BLUE)
        arcade.draw_line(500, 505, 220, 260, arcade.color.IMPERIAL_BLUE)
        arcade.draw_line(520, 400, 320, 215, arcade.color.IMPERIAL_BLUE)
        arcade.draw_line(350, 625, 500, 500, arcade.color.IMPERIAL_BLUE)
        arcade.draw_line(250, 582.5, 517.5, 360, arcade.color.IMPERIAL_BLUE)
        arcade.draw_line(200, 505, 480, 260, arcade.color.IMPERIAL_BLUE)
        arcade.draw_line(180, 400, 380, 215, arcade.color.IMPERIAL_BLUE)

        # Draw the door
        arcade.draw_ellipse_filled(350, 250, 65, 125, arcade.color.LAVENDER_GRAY)
        arcade.draw_rectangle_filled(350, 100, 400, 225, arcade.color.DESERT_SAND)
        arcade.draw_circle_filled(350, 275, 20, arcade.color.ONYX)
        arcade.draw_line(350, 275, 350, 315, arcade.color.ONYX)
        arcade.draw_line(350, 275, 310, 275, arcade.color.ONYX)
        arcade.draw_line(350, 275, 350, 235, arcade.color.ONYX)
        arcade.draw_line(350, 275, 390, 275, arcade.color.ONYX)

        # Draw bubbler and bubbles on side of the pineapple
        arcade.draw_rectangle_filled(500, 475, 100, 30, arcade.color.LAVENDER_GRAY)
        arcade.draw_rectangle_filled(550, 490, 20, 60, arcade.color.LAVENDER_GRAY)
        arcade.draw_rectangle_filled(550, 530, 40, 20, arcade.color.LAVENDER_GRAY)
        arcade.draw_circle_filled(455, 360, 45, arcade.color.IMPERIAL_BLUE)
        arcade.draw_circle_filled(455, 360, 25, arcade.color.WATERSPOUT)
        arcade.draw_circle_filled(260, 450, 45, arcade.color.IMPERIAL_BLUE)
        arcade.draw_circle_filled(260, 450, 25, arcade.color.WATERSPOUT)
        arcade.draw_circle_outline(550, 570, 10, arcade.color.BUBBLES)
        arcade.draw_circle_outline(570, 590, 7.5, arcade.color.BUBBLES)
        arcade.draw_circle_outline(530, 610, 7.5, arcade.color.BUBBLES)

        # Draw sidewalk
        arcade.draw_polygon_filled([[250, 1],
                                    [290, 210],
                                    [410, 210],
                                    [450, 1],
                                    [250, 1]],
                                   arcade.color.LICORICE)



        self.spongebob.draw()
        self.gary.draw()

        def update(self, delta_time):
            self.gary.update()

    def on_key_press(self, key, modifiers):
        """ Called whenever the user presses a key. """
        if key == arcade.key.LEFT:
            self.gary.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.RIGHT:
            self.gary.change_x = MOVEMENT_SPEED
        elif key == arcade.key.UP:
            self.gary.change_y = MOVEMENT_SPEED
        elif key == arcade.key.DOWN:
            self.gary.change_y = -MOVEMENT_SPEED

    def on_key_release(self, key, modifiers):
        if key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.change_x = 0
        elif key ==  arcade.key.UP or key == arcade.key.DOWN:
            self.change_y = 0

    def on_mouse_motion(self, x, y, dx, dy):
        self.spongebob.position_x = x
        self.spongebob.position_y = y

    def on_key_press(self, x, y, key, modifiers):
        if key == arcade.key.LEFT:
            arcade.play_sound(self.jingle1)

        if self.position_x > SCREEN_WIDTH - self.radius:
            arcade.play_sound(self.jingle2)

        if self.position_y > SCREEN_HEIGHT - self.radius:
            arcade.play_sound(self.jingle2)



def main():
    window = MyGame()
    arcade.run()


main()